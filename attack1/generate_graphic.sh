#!/bin/bash
# USAGE: ./generate_graphic.sh <modest file name> <parameter> <starting value> <end value> <incrementer> <precision>
# USAGE: ./generate_graphic.sh attack1.modest offset -0.5 0.5 0.05 0.99

error=$'ERROR: Illegal number of parameters: \nUSAGE: ./generate_graphic.sh <modest file name> <parameter> <starting value> <end value> <incrementer> <precision>'
if [ $# -ne 6 ]; then
	echo "$error"
	exit 1
fi

modest_program=$1
temp_name=${modest_program%.modest}
dir_name=${temp_name##*/}

if [[ ! -d "${dir_name}_analysis_results" ]]; then
	mkdir ${dir_name}_analysis_results
fi

# Obtaining name of the properties

# cat $1 prints the content of the modest file
# sed -n '/^property/p' prints only the lines that have the word "property" at the beginning
# wc -l counts the properties that the modest program has to analyse
number_properties=$(cat $1 | sed -n '/^property/p' | wc -l)
PROPERTY_FILE_LIST=() # ("P_Cold_Water_Level_Alarm" "P_Liquid_Level_Alarm" "P_Temp_Alarm" "P_Temp_Safety" "P_Liquid_Safety""P_Cold_Water_Safety" "P_Cold_Water_Deadlock" "P_Liquid_Level_Deadlock" "P_Temperature_Deadlock") 

# For each property, create the corresponding CSV file that will contain the points to plot in the line chart
for (( j=1; j<=number_properties; j++ )); do
	# cat $1 prints the content of the modest file
	# sed -n '/^property/p' prints only the lines that have the word "property" at the beginning
	# cut -d' ' -f 5 selects the fifth word of the lines which is the property's name
	# head -n $j | tail -n 1 are used for selecting the name of the property from the list returned by the cut
	property_name=$(cat $1 | sed -n '/^property/p' | cut -d' ' -f 2 | head -n $j | tail -n 1)
	PROPERTY_FILE_LIST[$j-1]=$property_name.csv
	touch ${dir_name}_analysis_results/${PROPERTY_FILE_LIST[$j-1]}
	chmod 777 ${dir_name}_analysis_results/${PROPERTY_FILE_LIST[$j-1]}
	echo "$2;Probability" > ${dir_name}_analysis_results/${PROPERTY_FILE_LIST[$j-1]}
done
for i in `seq $3 $5 $4`;
do
	t=${i/,/.}
	#t="$i | tr ',' '.'";
	echo $t
done
for i in `seq $3 $5 $4`;
do
	for index in ${!PROPERTY_FILE_LIST[*]}
	do
		t=${i/,/.}
		mono $HOME/Modest/modes.exe -E "$2=$t" -R Uniform --max-run-length 10000000 -S ASAP -C $6 $1 > .analysis_result.txt
		echo "$i;$(cat .analysis_result.txt | sed -n '/Estimated probability:/p' | cut -d' ' -f 7 | head -n $(($index+1)) | tail -n 1)"
		echo "$i;$(cat .analysis_result.txt | sed -n '/Estimated probability:/p' | cut -d' ' -f 7 | head -n $(($index+1)) | tail -n 1)" >> ${dir_name}_analysis_results/${PROPERTY_FILE_LIST[$index]}
	done	
done

# Executing pyhton script for drawing the line chart 
# $2 is the parameter that has varied during the analysis
# $dir_name is the name of the directory that contains all the csv file with the points to plot
# python3 generate_graphic.py $2 ${dir_name}_analysis_results

rm .analysis_result.txt

# rm -rf ${dir_name}_analysis_results

